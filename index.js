const express = require('express');
const { JsonWebTokenError } = require('jsonwebtoken');
const CatsFacts = require('./CatsFacts');
const app = express()
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const port = 3000
const generateMessage = require('./CatsFacts');
const hackNSA = require('./MrRobot');
const hackNsa = require('./MrRobot');
const models = require('./models')

const config = {
  secretkey: 'SuperSecretKey'
}

app.use(bodyParser.json())

// middleware example
// const authenticationMiddleware = (req, res, next) => {
//   if(req.headers,authentication === "344656767dhsbrurvrgsdedr"){
//     next();
//   }
//   else{
//     res.send({
//       error: "true"
//     }, 401);
//   }
// }

// middleware se pun toate cu virgula
app.get('/hello-world', /*authenticationMiddleware,*/ (req, res) => {
  const furminator = new CatsFacts();
  const body = furminator.getFact();
  furminator.factCount = 4;
  console.log(furminator.factCount);

  body.then(body => {
    //console.log(body);
    res.send(body.text);
  });
});


app.get('/hackNSA',async  (req, res) => {
  const furminator = new CatsFacts();
  const { password } = await hackNSA();
  const { text } = await furminator.getFact();

  res.send({
    password, 
    text
  });
});


const authorizationMiddleware = (req, res, next) => {
  const { authorization } = req.headers;

  console.log(authorization);
  //console.log()

  if(!authorization){
    res.send({
      status: 'not ok',
    }, 401);
  }

  const jwtToken = authorization.replace("Bearer", "");

  jwt.verify(jwtToken, config.secretkey, (err, decoded) => {
    if(err){
      //console.log(err);
      res.send({
        status: 'not ok',
      }, 401);
    } else{
      next();
    }
    return;
  });

  // if(authorization === "Bearer test"){
  //   next();
  // }else{
  //   res.send({
  //     status: 'not ok',
  //   }, 401);
  // }

}

app.post('/graphql', authorizationMiddleware, (req, res) => {
    res.send({
      status: 'ok',
    });
});


app.post('/graphql/public', (req, res) => {
  const {user, pass} = req;

  console.log("Eu sunt userul:");
  console.log(user);

  if(user === "Gogu" && pass === "parola") {
    //payload - date de care are nevoie clientul, ex: name
    // date se are acces rapid fara sa se faca apel la baza
    // secretOrPrivateKey - cheie folosita la codare si decodare
    jwt.sign({}, config.secretkey, (err, token) => {
      res.send({
        token,
      });
    })
  }else{
    res.send({
      status: 'not ok',
    }, 401);
  }
});

  // console.log(body);
  // res.send({
  //   status: 'ok',
  // });


// body-parser


// app.get('/users/:userId', async function(req, res){
//   const userId = req.params.userId;
//   const user = await models.User.findByPk(userId);

//   console.log('user', user);

//   res.send({
//    firstName: user.firstName,
//    lastName: user.lastName,
//    email: user.email
//   });

// })


app.post('/categories/:categoryId/product', async function(req, res) {
    const categoryId = req.params.categoryId;
    const category = await models.Category.findByPk(categoryId);
    const product = await category.createProduct({
      Name: 'name',
      Description: 'description',
      Price: 10,
    });
  
  res.send({
    status: 'ok',
  });
});

app.post('/post/:postId/tag/:tagId/associate', async (req, res) => {
  const { postId, tagId } = req.params;
  const post = await models.Post.findByPk(postId);
  const tag = await models.Tag.findByPk(tagId);
  console.log('tag', tag)
  await post.addTag(tag);
  res.send({
    status: 'ok',
  });
});

app.post('/user/:userId/product/:productId/associate', async (req, res) => {
  const { userId, productId } = req.params;
  const user = await models.User.findByPk(userId);
  const product = await models.Product.findByPk(productId);
  
await user.addProduct(product);
res.send({
  status: 'ok',
});
});

app.listen(port, () => {
  console.log(`server started`);
});

